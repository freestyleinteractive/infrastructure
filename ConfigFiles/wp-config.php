<?php
/**
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, WordPress Language, and ABSPATH. You can find more information
 * by visiting {@link http://codex.wordpress.org/Editing_wp-config.php Editing
 * wp-config.php} Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'database_name_here');

/** MySQL database username */
define('DB_USER', 'username_here');

/** MySQL database password */
define('DB_PASSWORD', 'password_here');

/** MySQL hostname */
define('DB_HOST', '10.0.0.26');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'wrjmxLxO-YC.f%FWYj3|jmvH9Rdp+h$%w4Y4y,AA1_^|&R|oG]T<ndA(g={KF|xi');
define('SECURE_AUTH_KEY',  'J?t*W )Qz_DBa/&E8<OMO|q?a(q<Q(}~}d|V.tk47zB46.6LmOU:->5*G%WH*cqT');
define('LOGGED_IN_KEY',    '808V2}&2x%JSu>9Sch#l4s*,./,i6T{@KJbH5 ;cGL(?2.SjDXXGPii,K(PCe@bZ');
define('NONCE_KEY',        'A14|L5s/cQ3mbuT,<3JT(P9+hh>#*#fU ;D}w]NHp+-A>,b;EM!xZemA|4`Ghn1p');
define('AUTH_SALT',        ':?g+Bm^9)11[}UO//H:+W,{XGE[k|D%#`]r)Aq2XA@UXCeV`{Z[gXYAIvxy+cVn|');
define('SECURE_AUTH_SALT', 'X@Kf*7wkyQD|[sWdj#~%c#s5$^V~-|7N7$l$BtU.1-.n)qv:?hl@(t[wzrRSX`}Y');
define('LOGGED_IN_SALT',   '}:5oP$MvZ>HolR2lS)>W(sBOS62z|xIj{<L+w_3.54I9Hjp`%Pqt:`#i>joy^d&5');
define('NONCE_SALT',       '/K<?w;)Qyxq(])s1KOJZs+&o}GDB0Spe1Y &R:X[Hz&+PN%Lp/?y&!?+WsjFM@`M');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * WordPress Localized Language, defaults to English.
 *
 * Change this to localize WordPress. A corresponding MO file for the chosen
 * language must be installed to wp-content/languages. For example, install
 * de_DE.mo to wp-content/languages and set WPLANG to 'de_DE' to enable German
 * language support.
 */
define('WPLANG', '');

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 */
define('WP_DEBUG', true);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
