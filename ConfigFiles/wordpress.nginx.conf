###########################################################################
#                NGINX Configuration for Freestyle Interactive            #
#                          Created by Paul Taylor                         #
###########################################################################

user  nobody;

worker_processes 1;
worker_rlimit_nofile 200000;

events {
	worker_connections 4000;
	use epoll;
	multi_accept on;
}

http {
    open_file_cache max=200000 inactive=20s;
	open_file_cache_valid 30s;
	open_file_cache_min_uses 2;
	open_file_cache_errors on;

	access_log off;
	sendfile on;
	tcp_nopush on;
	tcp_nodelay on;
	keepalive_timeout 30;
	keepalive_requests 100000;
	reset_timedout_connection on;
	client_body_timeout 10;
	send_timeout 2;

    	proxy_cache_path    /cache levels=1:2 keys_zone=my-cache:8m max_size=1g inactive=600m;
    	proxy_temp_path     /cache/tmp;

	include			mime.types;
    	default_type		application/octet-stream;

	server {
		listen				*:80;
		server_name			_;

        	proxy_buffer_size   4K;
        	proxy_buffers       8   32K;
		proxy_set_header	X-Real-IP	$remote_addr;
		proxy_set_header	Host		$http_host;

        	root    /var/www/wordpress/htdocs;
        	index   index.html index.htm index.php;

               	location / {
            		try_files $uri $uri/ /index.php;
        	}

		location ~ .php$ {
            		fastcgi_pass   unix:/tmp/php5-fpm.sock;
            		fastcgi_index  index.php;
            		fastcgi_param  SCRIPT_FILENAME  $document_root$fastcgi_script_name;
            		include        fastcgi_params;
        	}

        	location ~ /.ht {
            		deny  all;
        	}
	}
}