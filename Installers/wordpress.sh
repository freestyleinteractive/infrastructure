#!/bin/bash

echo -n "Database Server: "
read -e DATABASE_SERVER
echo -n "Database Name: "
read -e DATABASE_NAME
echo -n "Database Username: "
read -e DATABASE_USERNAME
echo -n "Database Password: "
read -e DATABASE_PASSWORD

echo "Installing Wordpress..."

mkdir -p /home/Freestyle
mkdir -p /var/www/wordpress/htdocs
cd /home/Freestyle

curl -L https://bitbucket.org/freestyleinteractive/infrastructure/src/801443916dcf71581de9e7a17ab7811a094dceec/Distribs/nginx-1.2.4.tar.gz?at=master | tar xz
curl -L https://bitbucket.org/freestyleinteractive/infrastructure/src/801443916dcf71581de9e7a17ab7811a094dceec/Distribs/yaoweibin-nginx_upstream_check_module-v0.1.6-30-g8ec8024.tar.gz?at=master | tar xz
curl -L https://bitbucket.org/freestyleinteractive/infrastructure/src/801443916dcf71581de9e7a17ab7811a094dceec/Distribs/gnosek-nginx-upstream-fair-a18b409.tar.gz?at=master | tar xz
curl -L https://bitbucket.org/freestyleinteractive/infrastructure/src/801443916dcf71581de9e7a17ab7811a094dceec/Distribs/headers-more-nginx-module-master.tar.gz?at=master | tar xz
curl -L https://bitbucket.org/freestyleinteractive/infrastructure/src/801443916dcf71581de9e7a17ab7811a094dceec/Distribs/LuaJIT-2.0.0.tar.gz?at=master | tar xz
curl -L https://bitbucket.org/freestyleinteractive/infrastructure/src/801443916dcf71581de9e7a17ab7811a094dceec/Distribs/ngx_devel_kit-0.2.18.tar.gz?at=master | tar xz
curl -L https://bitbucket.org/freestyleinteractive/infrastructure/src/801443916dcf71581de9e7a17ab7811a094dceec/Distribs/lua-nginx-module-0.7.14rc2.tar.gz?at=master | tar xz
curl -L https://bitbucket.org/freestyleinteractive/infrastructure/src/801443916dcf71581de9e7a17ab7811a094dceec/Distribs/echo-nginx-module-0.42.tar.gz?at=master | tar xz
curl -L http://wordpress.org/latest.tar.gz | tar xz
rm -rf *.tar.gz

yum -y install gcc automake autoconf libtool make pcre-devel zlib-devel openssl-devel patch
cd /home/Freestyle/LuaJIT-2.0.0/
make install PREFIX=/opt/LuaJIT

cd /home/Freestyle/nginx-1.2.4
export LUAJIT_LIB=/opt/LuaJIT/lib
export LUAJIT_INC=/opt/LuaJIT/include/luajit-2.0
cp /opt/LuaJIT/lib/libluajit-5.1.so.2 /lib64/

./configure --prefix=/etc/nginx --sbin-path=/etc/nginx/nginx --conf-path=/etc/nginx/nginx.conf --pid-path=/etc/nginx/nginx.pid --with-http_ssl_module --add-module=/home/Freestyle/headers-more-nginx-module-master --add-module=/home/Freestyle/ngx_devel_kit-0.2.18 --add-module=/home/Freestyle/lua-nginx-module-0.7.14rc2 --add-module=/home/Freestyle/echo-nginx-module-0.42
make -j2
make install

curl -L https://bitbucket.org/freestyleinteractive/infrastructure/raw/8adef31124e5a7d3958878207359d1a4e1b742ab/init.d/nginx > /etc/init.d/nginx
chmod +x /etc/init.d/nginx
chkconfig nginx on

rm -f /etc/nginx/*.default
rm -f /etc/nginx/nginx.conf

cd /home/Freestyle/wordpress
cp -r * /var/www/wordpress/htdocs/
cd /var/www/wordpress/htdocs/
curl -L https://bitbucket.org/freestyleinteractive/infrastructure/raw/c1e39e52a012dc8c67cafa59b24af73e3d45b684/ConfigFiles/wp-config.php > /var/www/wordpress/htdocs/wp-config.php
sed -i -e "s/10.0.0.26/$DATABASE_SERVER/" wp-config.php
sed -i -e "s/database_name_here/$DATABASE_NAME/" wp-config.php
sed -i -e "s/username_here/$DATABASE_USERNAME/" wp-config.php
sed -i -e "s/password_here/$DATABASE_PASSWORD/" wp-config.php

rpm -Uvh http://www.mirrorservice.org/sites/dl.fedoraproject.org/pub/epel/6/x86_64/epel-release-6-8.noarch.rpm
rpm -Uvh http://rpms.famillecollet.com/enterprise/remi-release-6.rpm
yum --enablerepo=remi -y install nginx php-fpm php-common php-mysql
sed -i -e "s/127.0.0.1:9000/\/tmp\/php5-fpm.sock/" /etc/php-fpm.d/www.conf
service php-fpm restart

curl -L https://bitbucket.org/freestyleinteractive/infrastructure/raw/c1320b576547ee2c9165f05ab468ba79824ce7e5/ConfigFiles/wordpress.nginx.conf > /etc/nginx/nginx.conf
service nginx start

rm -rf /home/Freestyle

mkdir -p /etc/nginx/ssl/
mkdir -p /cache/tmp

echo "Finished installing Wordpress."