#!/bin/bash
echo "Installing Nginx server..."

mkdir -p /home/Freestyle
cd /home/Freestyle
curl -L https://bitbucket.org/freestyleinteractive/infrastructure/raw/801443916dcf71581de9e7a17ab7811a094dceec/Distribs/nginx-1.2.4.tar.gz?at=master | tar xz
curl -L https://bitbucket.org/freestyleinteractive/infrastructure/raw/801443916dcf71581de9e7a17ab7811a094dceec/Distribs/yaoweibin-nginx_upstream_check_module-v0.1.6-30-g8ec8024.tar.gz?at=master | tar xz
curl -L https://bitbucket.org/freestyleinteractive/infrastructure/raw/801443916dcf71581de9e7a17ab7811a094dceec/Distribs/gnosek-nginx-upstream-fair-a18b409.tar.gz?at=master | tar xz
curl -L https://bitbucket.org/freestyleinteractive/infrastructure/raw/801443916dcf71581de9e7a17ab7811a094dceec/Distribs/headers-more-nginx-module-master.tar.gz?at=master | tar xz
curl -L https://bitbucket.org/freestyleinteractive/infrastructure/raw/801443916dcf71581de9e7a17ab7811a094dceec/Distribs/LuaJIT-2.0.0.tar.gz?at=master | tar xz
curl -L https://bitbucket.org/freestyleinteractive/infrastructure/raw/801443916dcf71581de9e7a17ab7811a094dceec/Distribs/ngx_devel_kit-0.2.18.tar.gz?at=master | tar xz
curl -L https://bitbucket.org/freestyleinteractive/infrastructure/raw/801443916dcf71581de9e7a17ab7811a094dceec/Distribs/lua-nginx-module-0.7.14rc2.tar.gz?at=master | tar xz
curl -L https://bitbucket.org/freestyleinteractive/infrastructure/raw/801443916dcf71581de9e7a17ab7811a094dceec/Distribs/echo-nginx-module-0.42.tar.gz?at=master | tar xz
rm -rf *.tar.gz

yum -y install gcc automake autoconf libtool make pcre-devel zlib-devel openssl-devel patch

cd /home/Freestyle/LuaJIT-2.0.0/
make install PREFIX=/opt/LuaJIT

cd /home/Freestyle/nginx-1.2.4
patch -p1 < /home/Freestyle/yaoweibin-nginx_upstream_check_module-8ec8024/check_1.2.2+.patch
cd /home/Freestyle/gnosek-nginx-upstream-fair-a18b409
patch -p2 < /home/Freestyle/yaoweibin-nginx_upstream_check_module-8ec8024/upstream_fair.patch
cd /home/Freestyle/nginx-1.2.4
export LUAJIT_LIB=/opt/LuaJIT/lib
export LUAJIT_INC=/opt/LuaJIT/include/luajit-2.0
cp /opt/LuaJIT/lib/libluajit-5.1.so.2 /lib64/

./configure --prefix=/etc/nginx --sbin-path=/etc/nginx/nginx --conf-path=/etc/nginx/nginx.conf --pid-path=/etc/nginx/nginx.pid --with-http_ssl_module --with-http_realip_module --add-module=/home/Freestyle/yaoweibin-nginx_upstream_check_module-8ec8024 --add-module=/home/Freestyle/gnosek-nginx-upstream-fair-a18b409 --add-module=/home/Freestyle/headers-more-nginx-module-master --add-module=/home/Freestyle/ngx_devel_kit-0.2.18 --add-module=/home/Freestyle/lua-nginx-module-0.7.14rc2 --add-module=/home/Freestyle/echo-nginx-module-0.42
make -j2
make install

curl -L https://bitbucket.org/freestyleinteractive/infrastructure/raw/8adef31124e5a7d3958878207359d1a4e1b742ab/init.d/nginx > /etc/init.d/nginx
chmod +x /etc/init.d/nginx
chkconfig nginx on

rm -f /etc/nginx/*.default
rm -f /etc/nginx/nginx.conf
rm -rf /home/Freestyle

mkdir -p /var/www/
mkdir -p /etc/nginx/ssl/
mkdir -p /cache/tmp

echo "Finished installing Nginx server."