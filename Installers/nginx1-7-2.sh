#!/bin/bash
echo "Installing Nginx server..."

mkdir -p /home/Freestyle
cd /home/Freestyle
curl -L https://bitbucket.org/freestyleinteractive/infrastructure/raw/e01c7493d7b225d909d31b72440dfbc73146e998/Distribs/nginx1-7-2/nginx-1.7.2.tar.gz?at=master | tar xz
curl -L https://bitbucket.org/freestyleinteractive/infrastructure/raw/e01c7493d7b225d909d31b72440dfbc73146e998/Distribs/nginx1-7-2/nginx_upstream_check_module-0.3.0.tar.gz?at=master | tar xz
curl -L https://bitbucket.org/freestyleinteractive/infrastructure/raw/e01c7493d7b225d909d31b72440dfbc73146e998/Distribs/gnosek-nginx-upstream-fair-a18b409.tar.gz?at=master | tar xz
curl -L https://bitbucket.org/freestyleinteractive/infrastructure/raw/e01c7493d7b225d909d31b72440dfbc73146e998/Distribs/headers-more-nginx-module-master.tar.gz?at=master | tar xz
curl -L https://bitbucket.org/freestyleinteractive/infrastructure/raw/e01c7493d7b225d909d31b72440dfbc73146e998/Distribs/LuaJIT-2.0.0.tar.gz?at=master | tar xz
curl -L https://bitbucket.org/freestyleinteractive/infrastructure/raw/e01c7493d7b225d909d31b72440dfbc73146e998/Distribs/ngx_devel_kit-0.2.18.tar.gz?at=master | tar xz
curl -L https://bitbucket.org/freestyleinteractive/infrastructure/raw/e01c7493d7b225d909d31b72440dfbc73146e998/Distribs/nginx1-7-2/lua-nginx-module-0.9.16.tar.gz?at=master | tar xz
curl -L https://bitbucket.org/freestyleinteractive/infrastructure/raw/e01c7493d7b225d909d31b72440dfbc73146e998/Distribs/echo-nginx-module-0.42.tar.gz?at=master | tar xz
rm -rf *.tar.gz

yum -y install gcc automake autoconf libtool make pcre-devel zlib-devel openssl-devel patch

cd /home/Freestyle/LuaJIT-2.0.0/
make install PREFIX=/opt/LuaJIT

cd /home/Freestyle/nginx-1.7.2
patch -p1 < /home/Freestyle/nginx_upstream_check_module-0.3.0/check_1.7.2+.patch
cd /home/Freestyle/gnosek-nginx-upstream-fair-a18b409
patch -p1 < /home/Freestyle/nginx_upstream_check_module-0.3.0/upstream_fair.patch
cd /home/Freestyle/nginx-1.7.2
export LUAJIT_LIB=/opt/LuaJIT/lib
export LUAJIT_INC=/opt/LuaJIT/include/luajit-2.0
cp /opt/LuaJIT/lib/libluajit-5.1.so.2 /lib64/

./configure  --prefix=/etc/nginx  --sbin-path=/etc/nginx/nginx --conf-path=/etc/nginx/nginx.conf --pid-path=/etc/nginx/nginx.pid --with-http_ssl_module --with-http_realip_module --add-module=/home/Freestyle/nginx_upstream_check_module-0.3.0 --add-module=/home/Freestyle/gnosek-nginx-upstream-fair-a18b409 --add-module=/home/Freestyle/headers-more-nginx-module-master --add-module=/home/Freestyle/ngx_devel_kit-0.2.18 --add-module=/home/Freestyle/lua-nginx-module-0.9.16 --add-module=/home/Freestyle/echo-nginx-module-0.42
make -j2
make install

curl -L https://bitbucket.org/freestyleinteractive/infrastructure/raw/8adef31124e5a7d3958878207359d1a4e1b742ab/init.d/nginx > /etc/init.d/nginx
chmod +x /etc/init.d/nginx
chkconfig nginx on

rm -f /etc/nginx/*.default
rm -f /etc/nginx/nginx.conf
rm -rf /home/Freestyle

mkdir -p /var/www/
mkdir -p /etc/nginx/ssl/
mkdir -p /cache/tmp

echo "Finished installing Nginx server."